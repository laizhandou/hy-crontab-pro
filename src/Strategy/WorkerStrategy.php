<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Laizhandou\HyCrontabPro\Strategy;

use Carbon\Carbon;
use Laizhandou\HyCrontabPro\Crontab;
use Laizhandou\HyCrontabPro\CrontabLogout;
use Laizhandou\HyCrontabPro\PipeMessage;
use Laizhandou\HyCrontabPro\LogoutPipeMessage;
use Hyperf\Server\ServerFactory;
use Psr\Container\ContainerInterface;
use Swoole\Server;

class WorkerStrategy extends AbstractStrategy
{
    /**
     * @var ServerFactory
     */
    protected $serverFactory;

    /**
     * @var int
     */
    protected $currentWorkerId = -1;

    public function __construct(ContainerInterface $container)
    {
        $this->serverFactory = $container->get(ServerFactory::class);
    }

    public function dispatch(Crontab $crontab)
    {
        $server = $this->serverFactory->getServer()->getServer();
        if ($server instanceof Server && $crontab->getExecuteTime() instanceof Carbon) {
            $workerId = $this->getNextWorkerId($server);
            $server->sendMessage(new PipeMessage(
                'callback',
                [ExecutorManager::class, 'execute'],
                $crontab
            ), $workerId);
        }
    }

    public function logout(array $crontab)
    {
        if (!$crontab) {
            return;
        }
        $server = $this->serverFactory->getServer()->getServer();
        if ($server instanceof Server) {
            for ($workerId = 0; $workerId < $server->setting['worker_num']; $workerId++) {
                $server->sendMessage(new LogoutPipeMessage(
                    'callback',
                    [ExecutorManager::class, 'logout'],
                    new CrontabLogout($crontab)
                ), $workerId);
            }
        }
    }

    protected function getNextWorkerId(Server $server): int
    {
        ++$this->currentWorkerId;
        $maxWorkerId = $server->setting['worker_num'] - 1;
        if ($this->currentWorkerId > $maxWorkerId) {
            $this->currentWorkerId = 0;
        }
        return $this->currentWorkerId;
    }
}
