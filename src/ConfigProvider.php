<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Laizhandou\HyCrontabPro;

use Laizhandou\HyCrontabPro\Configure\ConfigConfigure;
use Laizhandou\HyCrontabPro\Configure\ConfigureInterface;
use Laizhandou\HyCrontabPro\Listener\CrontabRegisterListener;
use Laizhandou\HyCrontabPro\Listener\OnPipeMessageListener;
use Laizhandou\HyCrontabPro\Strategy\StrategyInterface;
use Laizhandou\HyCrontabPro\Strategy\WorkerStrategy;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                StrategyInterface::class => WorkerStrategy::class,
                ConfigureInterface::class => ConfigConfigure::class
            ],
            'listeners' => [
//                CrontabRegisterListener::class,
                OnPipeMessageListener::class,
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'The config for crontab.',
                    'source' => __DIR__ . '/../publish/crontab.php',
                    'destination' => BASE_PATH . '/config/autoload/crontab.php',
                ],
            ],
        ];
    }
}
